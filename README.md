# CAB302 Assignment - Java Design Tool

A fictious company makes use of plotters - devices used for drawing vector-based graphics onto physical material from an input file. These files called VEC files are 
not used by any hardware or software on the market. Our task was to design a piece of Java software, from scratch, allowing for the previewing of and an editing of VEC files;
much like tools similiar to MS Paint.

## Getting Started

### Prerequisites
1. JavaFX 11 Installed
2. Add JavaFX to modules

### Troubleshooting

1. If Kotlin Error; Rebuild Project



## Built With

* [Getting Started with JavaFX 12](https://openjfx.io/openjfx-docs/) - GUI tool used
* [IntelliJ](https://www.jetbrains.com/idea/) - IDE used



## Authors

* **Hugh Duong-Tran-Tien** - *Developer* - [hughdtt](https://gitlab.com/hughdtt)
* **Henry Shi Jia Nguyen** - *Developer* - [hnguy59](https://gitlab.com/hnguy59)



## Acknowledgments

* **Helped us get started** - [JavaFX Sample for IntelliJ](https://github.com/hughdtt/samples/tree/master/IDE/IntelliJ/Non-Modular/Java)
* **Queensland University of Technology for such an amazing assignment!**


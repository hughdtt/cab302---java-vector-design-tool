package Editor;

import javafx.scene.control.Alert;

public class Alertbox{
    public static void alert(String s){
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setContentText(s);
        a.show();
    }
}

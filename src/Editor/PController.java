package Editor;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class PController {
    private static int Iwidth;
    private static int Iheight;

    public static boolean isNumeric(String strNum){
        try {
            double d = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }



    static boolean answer = false;

    public static boolean display(){
        //create export button
        Button export = new Button("Export");

        //create cancel button
        Button cancel = new Button("Cancel");

        //create help label
        Label label = new Label("Select");
        Label label2 = new Label("your exported size");
        label.setFont(Font.font("Ariel",  15));
        label2.setFont(Font.font("Ariel", 15));

        //create width and height label
        Label widthLabel = new Label("Width:");
        Label heightLabel = new Label("Height:");

        //create enter width and height
        TextField width = new TextField("450");
        TextField height = new TextField("450");

        //new stage
        Stage window = new Stage();

        //settings for stage
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Export to Bitmap");
        window.setMinHeight(200);
        window.setMinWidth(400);

        //export
        export.setOnAction(e -> {
            //save width height
            if (isNumeric(width.getCharacters().toString()) &&
                isNumeric(height.getCharacters().toString())) {
                int widthn = Integer.parseInt(width.getCharacters().toString());
                int heightn = Integer.parseInt(height.getCharacters().toString());
                if (0 < widthn && widthn < Integer.MAX_VALUE || 0 < heightn && heightn < Integer.MAX_VALUE) {
                    Iwidth = widthn;
                    Iheight = heightn;
                    answer = true;
                    window.close();
                }
            }else{
                Alertbox.alert("Please enter an integer between 1 and " + Integer.MAX_VALUE);
            }
        });



        //cancel
        cancel.setOnAction(e -> {
            answer = false;
            window.close();
        });

        // layout
        GridPane layout = new GridPane();
        layout.setMinSize(400,200);
        layout.setHgap(5);
        layout.setVgap(5);
        layout.setPadding(new Insets(10,10,10,10));

        layout.setAlignment(Pos.CENTER);

        // add every function
        layout.add(label, 0,0);
        layout.add(label2, 1, 0);
        layout.add(widthLabel, 0, 1);
        layout.add(heightLabel, 0, 2);
        layout.add(width, 1, 1);
        layout.add(height, 1, 2);
        layout.add(export, 0, 3);
        layout.add(cancel, 1, 3);


        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }

    public static int getWidth(){
        return Iwidth;
    }
    public static int getHeight(){
        return Iheight;
    }
}

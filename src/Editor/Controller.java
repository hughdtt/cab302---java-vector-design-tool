package Editor;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    //>>>>>>>>>>>>>>>>>>>>>>>FXML Variables<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @FXML
    private RadioButton penRB, fillRB;
    @FXML
    private ColorPicker fillColourPick, strokeColourPick;
    @FXML
    private Canvas ActualCanvas;
    @FXML
    private Canvas EffectCanvas;
    @FXML
    private Canvas WhiteCanvas;
    @FXML
    private BorderPane borderPane;
    @FXML
    private Pane pane;
    @FXML
    private Button plotBtn, rectBtn, lineBtn, ellipseBtn, polyBtn;
    @FXML
    private Label label,labelXY;
    @FXML
    private Button Export, Cancel;
    @FXML
    private TextField Width, Height;
    @FXML
    private Slider sizeSlider;

    //////////////////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>>>Draw variables<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    private GraphicsContext actualPointer, effectPointer, whitePointer;
    private boolean drawPlot = true, drawLine = false, drawEllipse = false, drawRect = false, drawPolygon = false;
    double startX, startY, endX, endY, storeX, storeY;
    ArrayList<Double> polyX = new ArrayList<Double>();
    ArrayList<Double> polyY = new ArrayList<Double>();

    //////////////////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>>>Load and Save Array<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    ArrayList<String> terminal = new ArrayList<String>();
    ArrayList<String> history = new ArrayList<String>();
    Desktop desktop = Desktop.getDesktop();
    boolean update = false;
    boolean onLoad = false;
    boolean saved = false;
    String filePath = "";
    ArrayList<String> store = new ArrayList<String>();
    //////////////////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>>>Mouse Events<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @FXML
    private void onMouseEnteredListener(MouseEvent e){
        DecimalFormat df = new DecimalFormat("#.##");
        String formatX = df.format(e.getX()/ ActualCanvas.getWidth());
        String formatY = df.format(e.getY()/ ActualCanvas.getHeight());
        labelXY.setText("(" + formatX + "," + formatY + ")");
    }

    @FXML
    private void onMousePressedListener(MouseEvent e) {

        //Stores 2 variables for x and y
        //StoreX & Y useful for rectangle/ellipse functions
        this.startX = e.getX();
        this.startY = e.getY();
        this.storeX = e.getX();
        this.storeY = e.getY();
    }

    @FXML
    private void onMouseDraggedListener(MouseEvent e) {
        this.endX = e.getX();
        this.endY = e.getY();

        //Based on which is true, draws relative shape effect as guide.
        if (drawPlot) {
            drawPlotEffect();
            label.setText("Selected: Plot");
        }
        if (drawRect) {
            drawRectEffect();
            label.setText("Selected: Rectangle");
        }
        if (drawEllipse) {
            drawEllipseEffect();
            label.setText("Selected: Ellipse");
        }
        if (drawLine) {
            drawLineEffect();
            label.setText("Selected: Line");
        }
    }

    @FXML
    private void onMouseReleaseListener(MouseEvent e) {
        this.endX = e.getX();
        this.endY = e.getY();

        //Based on which is true, draws relative shape.
        if (drawPlot) {
            actualPointer.setStroke(strokeColourPick.getValue());
            drawPlot(endX, endY);
            pc("PLOT");
        }
        if (drawRect) {
            if (fillRB.isSelected()) {
                actualPointer.setFill(fillColourPick.getValue());
                actualPointer.setStroke(strokeColourPick.getValue());
                drawRect(storeX, storeY, endX, endY, true);

            } else {
                actualPointer.setStroke(strokeColourPick.getValue());
                drawRect(storeX, storeY, endX, endY, false);
            }

            pc("RECTANGLE");

        }
        if (drawEllipse) {
            if (fillRB.isSelected()) {
                actualPointer.setFill(fillColourPick.getValue());
                actualPointer.setStroke((strokeColourPick.getValue()));
                drawEllipse(storeX, storeY, endX, endY, true);
            } else {
                actualPointer.setStroke(strokeColourPick.getValue());
                drawEllipse(storeX, storeY, endX, endY, false);
            }

            pc("ELLIPSE");
        }
        if (drawLine) {
            actualPointer.setStroke(strokeColourPick.getValue());
            drawLine(storeX, storeY, endX, endY);
            pc("LINE");
        }
        if (drawPolygon) {
            drawPlot(endX, endY);
            label.setText("Selected: Poly \tPlot the points, ESC to connect");
        }
        polyX.add(e.getX());
        polyY.add(e.getY());


    }

    @FXML
    private void onEscapePressedListener(KeyEvent k) {
        final KeyCombination keyCombCtrlZ = new KeyCodeCombination(KeyCode.Z,
                KeyCombination.CONTROL_DOWN);
        final KeyCombination keyCombCtrlY = new KeyCodeCombination(KeyCode.Y,
                KeyCombination.CONTROL_DOWN);
        if (drawPolygon == true && k.getCode() == KeyCode.ESCAPE) {
            if (fillRB.isSelected()) {
                actualPointer.setFill(fillColourPick.getValue());
                actualPointer.setStroke(strokeColourPick.getValue());
                drawPolygon(true);
            } else {
                actualPointer.setStroke(strokeColourPick.getValue());
                drawPolygon(false);
            }
            label.setText("Selected: Poly \tComplete");
            pc("POLYGON");
            polyX.clear();
            polyY.clear();
        }

        if (keyCombCtrlZ.match(k)) {
            try {
                undoF();
            } catch (Exception ex) {
                label.setText("Error: Nothing left to Undo");
            }

        }

        if (keyCombCtrlY.match(k)) {
            try {
                history.add(store.get(store.size() - 1));
                store.remove(store.size() - 1);
                label.setText("Redo");
                redraw();
                Load(history);
            } catch (Exception ex) {
                label.setText("Error: Nothing left to Redo");
            }

        }

    }

    //////////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>Draw methods<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    private void undoF(){
        store.add(history.get(history.size() - 1));
        history.remove(history.size() - 1);
        redraw();
        Load(history);
        label.setText("Undo");
    }

    private void pc(String s) {
        //Prints Vector
        penColour();
        fillColour();

        print(s);
    }

    private void print(String shape) {
        //Print out Vector
        if (shape == "PLOT") {
            float fEndX = (float) ((float) endX / ActualCanvas.getWidth());
            float fEndY = (float) ((float) endY / ActualCanvas.getHeight());

            String print = String.format(shape + " " + fEndX + " " + fEndY);
            terminal.add(print);
            history.add(print);
        } else if (shape == "POLYGON") {

            StringBuilder polyVector = new StringBuilder();
            for (int i = 0; i < polyX.size(); i++) {
                //DecimalFormat df = new DecimalFormat("#.##");
//                String formatX = df.format(polyX.get(i)/ActualCanvas.getWidth());
//                String formatY = df.format(polyY.get(i)/ActualCanvas.getHeight());
                polyVector.append(" " + polyX.get(i) / ActualCanvas.getWidth() + " " + polyY.get(i) / ActualCanvas.getHeight());
            }

            String print = String.format(shape + polyVector);
            terminal.add(print);
            history.add(print);
        } else {
            float fStartX = (float) ((float) storeX / ActualCanvas.getWidth());
            float fStartY = (float) ((float) storeY / ActualCanvas.getHeight());
            float fEndX = (float) ((float) endX / ActualCanvas.getWidth());
            float fEndY = (float) ((float) endY / ActualCanvas.getHeight());

            String print = String.format(shape + " " + fStartX + " " + fStartY + " " + fEndX + " " + fEndY);
            terminal.add(print);
            history.add(print);
        }
    }

    private void penColour() {
        String rgb = strokeColourPick.getValue().toString();
        String print = "PEN #" + rgb.substring(2, 8);
        terminal.add(print);
        history.add(print);
    }

    private void fillColour() {
        String print;
        String rgb;
        if (fillRB.isSelected()) {
            rgb = fillColourPick.getValue().toString();
            print = "FILL #" + rgb.substring(2, 8);
        } else {
            print = "FILL OFF";
        }

        terminal.add(print);
        history.add(print);
    }

    private void drawPlot(double x, double y) {
        //Line Width
        actualPointer.setLineWidth(2);
        //Draws Point
        actualPointer.strokeLine(x, y, x, y);
    }

    private void drawLine(double x, double y, double x1, double y1) {
        //Same as above except takes in account drag.
        actualPointer.setLineWidth(1);
        actualPointer.strokeLine(x, y, x1, y1);
    }

    private void drawRect(double startX, double startY, double endX, double endY, boolean fill) {
        double width = endX - startX;
        double height = endY - startY;

        if (width < 0) {
            width = -width;
            startX = endX;
        }
        if (height < 0) {
            height = -height;
            startY = endY;
        }
        if (fill) {
            actualPointer.fillRect(startX, startY, width, height);

            actualPointer.setLineWidth(1);
            actualPointer.strokeRect(startX, startY, width, height);
        } else {
            actualPointer.setLineWidth(1);
            actualPointer.strokeRect(startX, startY, width, height);
        }

    }

    private void drawEllipse(double startX, double startY, double endX, double endY, boolean fill) {
        double width = endX - startX;
        double height = endY - startY;

        if (width < 0) {
            width = -width;
            startX = endX;
        }
        if (height < 0) {
            height = -height;
            startY = endY;
        }
        actualPointer.setLineWidth(1);
        if (fill) {
            actualPointer.fillOval(startX, startY, width, height);
            actualPointer.strokeOval(startX, startY, width, height);
        } else {
            actualPointer.strokeOval(startX, startY, width, height);
        }
    }

    //Converts ArrayList to Double Array; Used for drawPolygon
    public static double[] convertDouble(ArrayList<Double> list) {
        double[] x = new double[list.size()];
        for (int i = 0; i < x.length; i++) {
            x[i] = list.get(i).intValue();
        }
        return x;
    }

    private void drawPolygon(boolean fill) {
        actualPointer.setLineWidth(1);
        actualPointer.setStroke(strokeColourPick.getValue());

        if (fill) {
            actualPointer.fillPolygon(convertDouble(polyX), convertDouble(polyY), polyX.size());
            actualPointer.strokePolygon(convertDouble(polyX), convertDouble(polyY), polyX.size());
        } else {
            actualPointer.strokePolygon(convertDouble(polyX), convertDouble(polyY), polyX.size());
        }

    }


    //////////////////////////////////////////////////////////////////////
    //>>>>>>>>>>>>>>>>>>>>>>>>>>Draw effects methods<<<<<<<<<<<<<<<<<<<<<<<

    private void drawPlotEffect() {
        effectPointer.setLineWidth(1);
        effectPointer.setStroke(strokeColourPick.getValue());
        effectPointer.clearRect(0, 0, EffectCanvas.getWidth(), EffectCanvas.getHeight());
        effectPointer.strokeLine(endX, endY, endX, endY);
    }

    private void drawLineEffect() {
        effectPointer.setLineWidth(1);
        effectPointer.setStroke(strokeColourPick.getValue());
        effectPointer.clearRect(0, 0, EffectCanvas.getWidth(), EffectCanvas.getHeight());
        effectPointer.strokeLine(startX, startY, endX, endY);
    }

    private void drawRectEffect() {
        double width = endX - storeX;
        double height = endY - storeY;

        if (width < 0) {
            width = -width;
            startX = endX;
        }
        if (height < 0) {
            height = -height;
            startY = endY;
        }
        effectPointer.setLineWidth(1);

        if (fillRB.isSelected()) {
            effectPointer.clearRect(0, 0, EffectCanvas.getWidth(), EffectCanvas.getHeight());
            effectPointer.setFill(fillColourPick.getValue());
            effectPointer.fillRect(startX, startY, width, height);

            effectPointer.setStroke(strokeColourPick.getValue());
            effectPointer.setLineWidth(1);
            effectPointer.strokeRect(startX, startY, width, height);
        } else {
            effectPointer.clearRect(0, 0, EffectCanvas.getWidth(), EffectCanvas.getHeight());
            effectPointer.setStroke(strokeColourPick.getValue());
            if (width < 0) {
                effectPointer.strokeRect(startX, startY, -width, height);
            } else {
                effectPointer.strokeRect(startX, startY, width, height);
            }
        }
    }

    private void drawEllipseEffect() {
        double width = endX - storeX;
        double height = endY - storeY;

        if (width < 0) {
            width = -width;
            startX = endX;
        }
        if (height < 0) {
            height = -height;
            startY = endY;
        }

        effectPointer.setLineWidth(1);

        if (fillRB.isSelected()) {
            effectPointer.clearRect(0, 0, EffectCanvas.getWidth(), EffectCanvas.getHeight());
            effectPointer.setFill(fillColourPick.getValue());
            effectPointer.fillOval(startX, startY, width, height);

            effectPointer.setStroke(strokeColourPick.getValue());
            effectPointer.setLineWidth(1);
            effectPointer.strokeOval(startX, startY, width, height);
        } else {
            effectPointer.clearRect(0, 0, EffectCanvas.getWidth(), EffectCanvas.getHeight());
            effectPointer.setStroke(strokeColourPick.getValue());
            effectPointer.strokeOval(startX, startY, width, height);
        }
    }

    ///////////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>Buttons Set Current<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @FXML
    private void setCurrentPlot(ActionEvent e) {
        //Sets Plot True and Everything Else False
        drawPlot = true;
        drawLine = false;
        drawEllipse = false;
        drawRect = false;
        drawPolygon = false;

        label.setText("Selected: Plot");
    }

    @FXML
    private void setCurrentLine(ActionEvent e) {
        //Sets Line True and Everything Else False
        drawPlot = false;
        drawLine = true;
        drawRect = false;
        drawEllipse = false;
        drawPolygon = false;

        label.setText("Selected: Line");
    }

    @FXML
    private void setCurrentRectangle(ActionEvent e) {
        //Sets Rectangle True and Everything Else False
        drawPlot = false;
        drawLine = false;
        drawRect = true;
        drawEllipse = false;
        drawPolygon = false;

        label.setText("Selected: Rectangle");

    }

    @FXML
    private void setCurrentEllipse(ActionEvent e) {
        //Sets Ellipse True and Everything Else False
        drawPlot = false;
        drawLine = false;
        drawRect = false;
        drawEllipse = true;
        drawPolygon = false;

        label.setText("Selected: Ellipse");
    }

    @FXML
    private void setCurrentPolygon(ActionEvent e) {
        //Sets Polygon True and Everything Else False
        drawPlot = false;
        drawLine = false;
        drawEllipse = false;
        drawRect = false;
        drawPolygon = true;

        label.setText("Selected: Poly \tPlot the points, ESC to connect");

        polyX.clear();
        polyY.clear();

    }

    @FXML
    private void clearCanvas(ActionEvent e) {
        //Resets Both Canvas
        terminal.clear();
        history.add("RECTANGLE 0 0 0 0");
        actualPointer.clearRect(0, 0, ActualCanvas.getHeight() * 2, ActualCanvas.getHeight() * 2);
        effectPointer.clearRect(0, 0, ActualCanvas.getWidth() * 2, ActualCanvas.getHeight() * 2);

        whitePointer.setFill(Color.WHITE);
        whitePointer.fillRect(0, 0, ActualCanvas.getWidth(), ActualCanvas.getHeight());

        onLoad = false;
        label.setText("Cleared!");
    }

    private void redraw() {
        actualPointer.clearRect(0, 0, ActualCanvas.getHeight() * 2, ActualCanvas.getHeight() * 2);
        effectPointer.clearRect(0, 0, ActualCanvas.getWidth() * 2, ActualCanvas.getHeight() * 2);
        whitePointer.clearRect(0, 0, ActualCanvas.getWidth() * 2, ActualCanvas.getHeight() * 2);

        whitePointer.setFill(Color.WHITE);
        whitePointer.fillRect(0, 0, ActualCanvas.getWidth(), ActualCanvas.getHeight());


        reset();

    }


    //////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>Load method<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    private void Load(ArrayList<String> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            String command = arrayList.get(i);

            String[] array;
            array = command.split(" ");

            if (array[0].contains("PEN")) {
                actualPointer.setStroke(Color.web(array[1]));
            }
            if (array[0].contains("FILL")) {
                if (array[1].contains("OFF")) {
                    fillRB.setSelected(false);
                } else {
                    fillRB.setSelected(true);
                    actualPointer.setFill(Color.web(array[1]));
                }
            }
            if (array[0].contains("PLOT")) {
                Double x = Double.valueOf(array[1]) * ActualCanvas.getWidth();
                Double y = Double.valueOf(array[2]) * ActualCanvas.getHeight();
                drawPlot(x, y);
            }
            if (array[0].contains("LINE")) {
                Double x = Double.valueOf(array[1]) * ActualCanvas.getWidth();
                Double y = Double.valueOf(array[2]) * ActualCanvas.getHeight();
                Double x1 = Double.valueOf(array[3]) * ActualCanvas.getWidth();
                Double y1 = Double.valueOf(array[4]) * ActualCanvas.getHeight();
                drawLine(x, y, x1, y1);
            }
            if (array[0].contains("RECTANGLE")) {

                Double x = Double.valueOf(array[1]) * ActualCanvas.getWidth();
                Double y = Double.valueOf(array[2]) * ActualCanvas.getHeight();
                Double x1 = Double.valueOf(array[3]) * ActualCanvas.getWidth();
                Double y1 = Double.valueOf(array[4]) * ActualCanvas.getHeight();
                if (fillRB.isSelected()) {
                    drawRect(x, y, x1, y1, true);
                } else {
                    drawRect(x, y, x1, y1, false);
                }

            }
            if (array[0].contains("ELLIPSE")) {
                Double x = Double.valueOf(array[1]) * ActualCanvas.getWidth();
                Double y = Double.valueOf(array[2]) * ActualCanvas.getHeight();
                Double x1 = Double.valueOf(array[3]) * ActualCanvas.getWidth();
                Double y1 = Double.valueOf(array[4]) * ActualCanvas.getHeight();
                if (fillRB.isSelected()) {
                    drawEllipse(x, y, x1, y1, true);
                } else {
                    drawEllipse(x, y, x1, y1, false);
                }
            }
            if (array[0].contains("POLYGON")) {
                polyX.clear();
                polyY.clear();
                for (int k = 1; k < array.length; k = k + 2) {
                    polyX.add(Double.valueOf(array[k]) * ActualCanvas.getWidth());
                }
                for (int j = 2; j < array.length; j = j + 2) {
                    polyY.add(Double.valueOf(array[j]) * ActualCanvas.getHeight());
                }
                if (fillRB.isSelected()) {
                    drawPolygon(true);
                } else {
                    drawPolygon(false);
                }
                polyX.clear();
                polyY.clear();

            }
        }
    }

    private void reset() {
        onLoad = false;
        actualPointer.setStroke(Color.BLACK);
        actualPointer.setFill(Color.WHITE);
        penRB.setSelected(true);
        fillRB.setSelected(false);

        drawPlot = true;
        drawLine = false;
        drawEllipse = false;
        drawRect = false;
        drawPolygon = false;
    }

    //////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>Open Save and Exit<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @FXML
    private void onNew(ActionEvent e) {
        saved = false;
        filePath = "";
        terminal.clear();
        history.clear();
        redraw();
        reset();
        label.setText("New Canvas!");
    }

    @FXML
    private void onOpen(ActionEvent e) {
        history.clear();
        FileChooser filechooser = new FileChooser();
        filechooser.setTitle("Open Resource File");
        filechooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("VEC files (*.vec)", "*.vec"));
        File fileOpen = filechooser.showOpenDialog(null);


        if (fileOpen != null) {
            try {
                redraw();
                filePath = fileOpen.getAbsolutePath();
                FileReader file = new FileReader(filePath);
                BufferedReader reader = new BufferedReader(file);
                terminal.clear();
                String line = reader.readLine();
                while (line != null) {
                    terminal.add(line);
                    history.add(line);
                    line = reader.readLine();
                }
                reader.close();
                fileOpen.createNewFile();

                onLoad = true;
                redraw();
                Load(terminal);
                reset();
                label.setText("Open Successfully!");

            } catch (IOException ex) {
                label.setText("Error: Failed to Open");
            }
            saved = true;
        }
    }

    @FXML
    private void onSave(ActionEvent e) {

        if (saved = true) {
            try {
                BufferedWriter outputWriter = null;
                outputWriter = new BufferedWriter(new FileWriter(filePath));
                for (int i = 0; i < terminal.size(); i++) {
                    outputWriter.write(terminal.get(i) + "\n");
                }
                outputWriter.flush();
                outputWriter.close();
                label.setText("Save Complete!");
            } catch (IOException ex) {
                Alertbox.alert("Please Save As first");
                label.setText("Error: Please Save As first");

            }
        } else {

            label.setText("Error: Please Save As first");
        }
    }

    @FXML
    private void onSaveAs(ActionEvent e) {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save as");

        //Set extension filter
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("VEC files (*.vec)", "*.vec"));

        //Prompt to select file
        File file = fileChooser.showSaveDialog(null);


        if (file != null) {
            try {
                BufferedWriter outputWriter = null;
                filePath = file.getAbsolutePath();
                outputWriter = new BufferedWriter(new FileWriter(filePath));
                for (int i = 0; i < terminal.size(); i++) {
                    outputWriter.write(terminal.get(i) + "\n");
                }

                outputWriter.flush();
                outputWriter.close();
                label.setText("Save Complete!");
            } catch (IOException ex) {
                label.setText("Error: Failed to Save");
            }
        }
        saved = true;
    }

    @FXML
    private void onSaveAsBMP(ActionEvent e) {
        boolean result = PController.display();
        if (result){
            FileChooser fileChooser = new FileChooser();

            //Set extension filter
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.bmp"));

            //Prompt to select file
            File file = fileChooser.showSaveDialog(null);

            if (file != null) {
                try {
                    WritableImage image = ActualCanvas.snapshot(new SnapshotParameters(), null);
                    BufferedImage resized = scale(SwingFXUtils.fromFXImage(image, null), PController.getWidth(), PController.getHeight());

                    ImageIO.write(resized, "png", file);

                    label.setText("Exported as BMP!");
                } catch (IOException ex) {
                    label.setText("Error: Failed to export");
                }
            }
        }
    }


    public static BufferedImage scale(BufferedImage src, int w, int h)
    {
        BufferedImage img =
                new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        int x, y;
        int ww = src.getWidth();
        int hh = src.getHeight();
        int[] ys = new int[h];
        for (y = 0; y < h; y++)
            ys[y] = y * hh / h;
        for (x = 0; x < w; x++) {
            int newX = x * ww / w;
            for (y = 0; y < h; y++) {
                int col = src.getRGB(newX, ys[y]);
                img.setRGB(x, y, col);
            }
        }
        return img;
    }

    @FXML
    private void onExit(ActionEvent e) {
        history.clear();
        Platform.exit();
    }
    //////////////////////////////////////////////////////////////////


    //>>>>>>>>>>>>>>>>>>>>>Undo and Redo<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @FXML
    private void onUndo(ActionEvent e) {
        try {
            undoF();
        } catch (Exception ex) {
            label.setText("Nothing left to Undo");
        }
    }

    @FXML
    private void onRedo(ActionEvent e) {
        try {
            terminal.add(store.get(store.size() - 1));
            store.remove(store.size() - 1);
            redraw();
            Load(history);
            label.setText("Redo");
        } catch (Exception ex) {
            label.setText("Nothing left to Redo");
        }
    }
    //////////////////////////////////////////////////////////////////

    //>>>>>>>>>>>>>>>>>>>>>Initialise<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Initialize Canvas and Slider Adjustments
        actualPointer = ActualCanvas.getGraphicsContext2D();
        effectPointer = EffectCanvas.getGraphicsContext2D();
        whitePointer = WhiteCanvas.getGraphicsContext2D();
        sizeSlider.setMin(1);
        sizeSlider.setMax(20);
        sizeSlider.setValue(5);

        //Set Colour Black
        fillColourPick.setValue(Color.GREY);
        strokeColourPick.setValue(Color.BLACK);
        redraw();

        //Listeners
        sizeSlider.valueProperty().addListener((ov, oldValue, newValue) -> {
                pane.setTranslateX(newValue.doubleValue()*25);
                redraw();
                Load(terminal);
        });

        borderPane.prefHeightProperty().addListener((ov, oldValue, newValue) -> {
            ActualCanvas.setWidth(newValue.doubleValue()- 200);
            EffectCanvas.setWidth(newValue.doubleValue()- 200);
            WhiteCanvas.setWidth(newValue.doubleValue()- 200);
            ActualCanvas.setHeight(newValue.doubleValue()- 200);
            EffectCanvas.setHeight(newValue.doubleValue()- 200);
            WhiteCanvas.setHeight(newValue.doubleValue()- 200);

            if(newValue.doubleValue() < 800){
                sizeSlider.setValue(1);
            } else if (newValue.doubleValue() < 1000){
                sizeSlider.setValue(5);
            } else {
                sizeSlider.setValue(10);
            }
            redraw();
            Load(history);
        });

        //Set Toolbar Status
        label.setText("Selected: Plot");
    }
}

    //////////////////////////////////////////////////////////////////

    //KIM IS THE BEST


